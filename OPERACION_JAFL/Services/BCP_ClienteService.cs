﻿using OPERACION_JAFL.Models;
using OPERACION_JAFL.Repositories;
using OPERACION_JAFL.Utils;

namespace OPERACION_JAFL.Services
{
    public class BCP_ClienteService : IBCP_ClienteService
    {
        private readonly IBCP_ClienteRepo clienteRepository;

        public BCP_ClienteService(IBCP_ClienteRepo personRepository)
        {
            this.clienteRepository = personRepository;
        }
        public BCP_Cliente ConsultaCliente(string ci)
        {
            string message = "";
            if (Validator.ValidatenumericString(ci, 12, ref message)){
                return clienteRepository.ConsultaCliente(ci);
            };
            return new BCP_Cliente() { Paterno = "Error: " + message};
        }

        public BCP_Cliente RegistroCliente(BCP_Cliente cliente)
        {
            string message = "";
            if (Validator.ValidateString(cliente?.Paterno, 30, ref message) && 
                Validator.ValidateString(cliente?.Materno, 30, ref message) && 
                Validator.ValidateString(cliente?.Nombres, 30, ref message) &&
                Validator.ValidatenumericString(cliente?.CI, 12, ref message) &&
                Validator.ValidateAge((DateTime)(cliente?.FechaDeNacimiento), ref message) && 
                Validator.ValidateString(cliente?.Genero, 30, ref message) &&
                Validator.ValiadtePhone((int)(cliente?.Celular), ref message) &&
                Validator.ValidateIngresos((decimal)(cliente?.NivelDeIngresos), ref message) && 
                Validator.ValidateEmail(cliente?.Correo, ref message) && 
                Validator.ValidateDate((DateTime)(cliente?.FechaDeRegistro), ref message) &&
                Validator.ValidateDate((DateTime)(cliente?.FechaDeActualizacion), ref message))
            {
                return clienteRepository.RegistroCliente(cliente);
            }
            return new BCP_Cliente { Paterno = "Error: " + message };
        }
    }
}
