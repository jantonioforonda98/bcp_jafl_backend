﻿using OPERACION_JAFL.Models;
using OPERACION_JAFL.Repositories;
using OPERACION_JAFL.Utils;

namespace OPERACION_JAFL.Services
{
    public class BCP_CuentaService : IBCP_CuentaService
    {
        private readonly IBCP_CuentaRepo cuentaRepository;

        public BCP_CuentaService(IBCP_CuentaRepo cuentaRepository)
        {
            this.cuentaRepository = cuentaRepository;
        }
        public BCP_Cuenta ConsultaCuenta(string cuenta)
        {
            string message = "";
            if (Validator.ValidatenumericString(cuenta, 14, ref message)){
                return cuentaRepository.ConsultaCuenta(cuenta);
            }
            return new BCP_Cuenta() { Cuenta = "Error: " + message };
        }

        public BCP_Cuenta RegistroCuenta(BCP_Cuenta cuenta)
        {
            string message = "";
            if (Validator.ValidatenumericString(cuenta?.Cuenta, 14, ref message) &&
                Validator.ValidatenumericString(cuenta?.CI, 12, ref message) &&
                Validator.ValidateDate((DateTime)(cuenta?.FechaDeRegistro), ref message) &&
                Validator.ValidateDate((DateTime)(cuenta?.FechaDeActualizacion), ref message))
            {
                return cuentaRepository.RegistroCuenta(cuenta);
            }
            return new BCP_Cuenta() { Cuenta = "Error: " + message };
        }
    }
}
