﻿using OPERACION_JAFL.Models;

namespace OPERACION_JAFL.Services
{
    public interface IBCP_CuentaService
    {
        public BCP_Cuenta RegistroCuenta(BCP_Cuenta cuenta);

        public BCP_Cuenta ConsultaCuenta(string cuenta);
    }
}
