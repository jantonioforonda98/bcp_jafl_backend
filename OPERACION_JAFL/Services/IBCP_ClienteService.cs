﻿using OPERACION_JAFL.Models;

namespace OPERACION_JAFL.Services
{
    public interface IBCP_ClienteService
    {
        public BCP_Cliente RegistroCliente(BCP_Cliente cliente);

        public BCP_Cliente ConsultaCliente(string ci);
    }
}
