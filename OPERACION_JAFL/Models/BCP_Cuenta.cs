﻿namespace OPERACION_JAFL.Models
{
    public class BCP_Cuenta
    {
        public string? Cuenta { get; set; }
        public string? CI { get; set; }
        public DateTime FechaDeRegistro { get; set; }
        public DateTime FechaDeActualizacion { get; set; }
    }
}
