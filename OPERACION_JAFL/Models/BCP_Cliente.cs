﻿namespace OPERACION_JAFL.Models
{
    public class BCP_Cliente
    {
        public string? Paterno { get; set; }
        public string? Materno { get; set; }
        public string? Nombres { get; set; }
        public string? CI { get; set; }
        public DateTime FechaDeNacimiento { get; set; }
        public string? Genero { get; set; }
        public int Celular { get; set; }
        public decimal NivelDeIngresos { get; set; }
        public string? Correo { get; set; }
        public DateTime FechaDeRegistro { get; set; }
        public DateTime FechaDeActualizacion { get; set; }
    }
}
