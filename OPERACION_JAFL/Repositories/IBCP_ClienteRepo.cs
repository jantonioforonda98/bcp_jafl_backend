﻿using OPERACION_JAFL.Models;

namespace OPERACION_JAFL.Repositories
{
    public interface IBCP_ClienteRepo
    {
        public BCP_Cliente RegistroCliente(BCP_Cliente cliente);

        public BCP_Cliente ConsultaCliente(string ci);

    }
}
