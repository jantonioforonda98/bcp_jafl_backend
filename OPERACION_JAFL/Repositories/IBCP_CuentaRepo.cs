﻿using OPERACION_JAFL.Models;

namespace OPERACION_JAFL.Repositories
{
    public interface IBCP_CuentaRepo
    {
        public BCP_Cuenta RegistroCuenta(BCP_Cuenta cuenta);

        public BCP_Cuenta ConsultaCuenta(string cuenta);
    }
}
