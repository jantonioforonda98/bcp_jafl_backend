﻿using Dapper;
using OPERACION_JAFL.Models;
using OPERACION_JAFL.Models.System;
using System.Data;
using System.Data.SqlClient;

namespace OPERACION_JAFL.Repositories
{
    public class BCP_CuentaRepo : IBCP_CuentaRepo
    {
        private readonly SQLConnection connection;

        public BCP_CuentaRepo(SQLConnection sqlConnection)
        {
            this.connection = sqlConnection;
        }
        public BCP_Cuenta ConsultaCuenta(string cuenta)
        {
            using IDbConnection db = new SqlConnection(this.connection.ConnectionString);
            var procedure = "Consulta_Cuenta";
            var parameters = new DynamicParameters();
            parameters.Add("@cuenta", cuenta, DbType.String, ParameterDirection.Input);
            return db.Query<BCP_Cuenta>(procedure, parameters, commandType: CommandType.StoredProcedure).First();
        }

        public BCP_Cuenta RegistroCuenta(BCP_Cuenta cuenta)
        {
            using IDbConnection db = new SqlConnection(this.connection.ConnectionString);
            var procedure = "Insert_Cuenta";
            var parameters = new DynamicParameters();
            parameters.Add("@cuenta", cuenta.Cuenta, DbType.String, ParameterDirection.Input);
            parameters.Add("@ci", cuenta.CI, DbType.String, ParameterDirection.Input);
            parameters.Add("@fechaDeRegistro", cuenta.FechaDeRegistro, DbType.DateTime, ParameterDirection.Input);
            parameters.Add("@fechaDeActualizacion", cuenta.FechaDeActualizacion, DbType.DateTime, ParameterDirection.Input);
            return db.Query<BCP_Cuenta>(procedure, parameters, commandType: CommandType.StoredProcedure).First();
        }
    }
}
