﻿using Dapper;
using OPERACION_JAFL.Models;
using OPERACION_JAFL.Models.System;
using System.Data;
using System.Data.SqlClient;

namespace OPERACION_JAFL.Repositories
{
    public class BCP_ClienteRepo : IBCP_ClienteRepo
    {
        private readonly SQLConnection connection;

        public BCP_ClienteRepo(SQLConnection sqlConnection)
        {
            this.connection = sqlConnection;
        }
        public BCP_Cliente ConsultaCliente(string ci)
        {
            using IDbConnection db = new SqlConnection(this.connection.ConnectionString);
            var procedure = "Consulta_Cliente";
            var parameters = new DynamicParameters();
            parameters.Add("@ci", ci, DbType.String, ParameterDirection.Input);
            return db.Query<BCP_Cliente>(procedure, parameters, commandType: CommandType.StoredProcedure).First();
        }

        public BCP_Cliente RegistroCliente(BCP_Cliente cliente)
        {
            using IDbConnection db = new SqlConnection(this.connection.ConnectionString);
            var procedure = "Insert_Cliente";
            var parameters = new DynamicParameters();
            parameters.Add("@paterno", cliente.Paterno, DbType.String, ParameterDirection.Input);
            parameters.Add("@materno", cliente.Materno, DbType.String, ParameterDirection.Input);
            parameters.Add("@nombres", cliente.Nombres, DbType.String, ParameterDirection.Input);
            parameters.Add("@ci", cliente.CI, DbType.String, ParameterDirection.Input);
            parameters.Add("@fechaDeNacimiento", cliente.FechaDeNacimiento, DbType.DateTime, ParameterDirection.Input);
            parameters.Add("@genero", cliente.Genero, DbType.String, ParameterDirection.Input);
            parameters.Add("@celular", cliente.Celular, DbType.Int32, ParameterDirection.Input);
            parameters.Add("@nivelDeIngresos", cliente.NivelDeIngresos, DbType.Decimal, ParameterDirection.Input);
            parameters.Add("@correo", cliente.Correo, DbType.String, ParameterDirection.Input);
            parameters.Add("@fechaDeRegistro", cliente.FechaDeRegistro, DbType.DateTime, ParameterDirection.Input);
            parameters.Add("@fechaDeActualizacion", cliente.FechaDeActualizacion, DbType.DateTime, ParameterDirection.Input);
            return db.Query<BCP_Cliente>(procedure, parameters, commandType: CommandType.StoredProcedure).First();
        }
    }
}
