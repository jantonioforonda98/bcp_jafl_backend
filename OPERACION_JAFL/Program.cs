using OPERACION_JAFL.Models.System;
using OPERACION_JAFL.Repositories;
using OPERACION_JAFL.Services;

var MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddCors(options =>
{
    options.AddPolicy(
        name: MyAllowSpecificOrigins,
        policy =>
        {
            policy.WithOrigins("http://localhost:3000").AllowAnyHeader().AllowAnyMethod();
        });
});

SQLConnection sqlConnection = builder.Configuration.GetSection("SQLConnection").Get<SQLConnection>();
builder.Services.AddScoped<IBCP_ClienteService, BCP_ClienteService>();
builder.Services.AddScoped<IBCP_ClienteRepo>(x => new BCP_ClienteRepo(sqlConnection));
builder.Services.AddScoped<IBCP_CuentaService, BCP_CuentaService>();
builder.Services.AddScoped<IBCP_CuentaRepo>(x => new BCP_CuentaRepo(sqlConnection));

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseCors(MyAllowSpecificOrigins);

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
