﻿using Microsoft.AspNetCore.Mvc;
using OPERACION_JAFL.Models;
using OPERACION_JAFL.Services;

namespace OPERACION_JAFL.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CuentaController : ControllerBase
    {
        private readonly ILogger<ClienteController> _logger;

        private readonly IBCP_CuentaService _cuentaService;

        public CuentaController(ILogger<ClienteController> logger, IBCP_CuentaService cuentaService)
        {
            _logger = logger;
            _cuentaService = cuentaService;
        }

        [HttpGet(Name = "GetCuenta")]
        public BCP_Cuenta GetCuenta(string cuenta)
        {
            try
            {
                return _cuentaService.ConsultaCuenta(cuenta);
            }
            catch
            {
                return new BCP_Cuenta() { Cuenta = "An error has occured" };
            }
        }

        [HttpPost(Name = "InsertCuenta")]
        public BCP_Cuenta PostCuenta(BCP_Cuenta cuenta)
        {
            try
            {
                return _cuentaService.RegistroCuenta(cuenta);
            }
            catch
            {
                return new BCP_Cuenta() { Cuenta = "An error has occured" };
            }
        }
    }
}