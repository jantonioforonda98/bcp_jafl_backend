using Microsoft.AspNetCore.Mvc;
using OPERACION_JAFL.Models;
using OPERACION_JAFL.Services;

namespace OPERACION_JAFL.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ClienteController : ControllerBase
    {
        private readonly ILogger<ClienteController> _logger;

        private readonly IBCP_ClienteService _clienteService;

        public ClienteController(ILogger<ClienteController> logger, IBCP_ClienteService clienteService)
        {
            _logger = logger;
            _clienteService = clienteService;
        }

        [HttpGet(Name = "GetCliente")]
        public BCP_Cliente GetCliente(string ci)
        {
            try
            {
                return _clienteService.ConsultaCliente(ci);
            }
            catch
            {
                return new BCP_Cliente() { Paterno = "An error has occured" };
            }
        }

        [HttpPost(Name = "InsertCliente")]
        public BCP_Cliente PostCliente(BCP_Cliente cliente)
        {
            try
            {
                return _clienteService.RegistroCliente(cliente);
            }
            catch(Exception ex)
            {
                return new BCP_Cliente() { Paterno = ex.Message };
            }
        }
    }
}