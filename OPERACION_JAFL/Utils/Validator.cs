﻿using System.Text.RegularExpressions;

namespace OPERACION_JAFL.Utils
{
    public static class Validator
    {
        public static bool ValidateString(string s, int length, ref string message)
        {
            message = "Nombres o genero invalidos";
            if (s == null || s == string.Empty)
            {
                return false;
            }
            if (s.Length > length)
            {
                return false;
            }
            if (Regex.IsMatch(s, @"^[a-zA-Z]+$")){
                return true;
            }
            return false;
        }
        public static bool ValidatenumericString(string s, int length, ref string message)
        {
            message = "Ci o cuenta invalidos";
            if (s == null || s == string.Empty)
            {
                return false;
            }
            if (s.Length > length)
            {
                return false;
            }
            if (Regex.IsMatch(s, @"^[0-9]+$")){
                return true;
            }
            return false;
        }

        public static bool ValidateAge(DateTime dob, ref string message)
        {
            DateTime now = DateTime.Now;
            int age = now.Year - dob.Year;
            message = "La fecha de nacimiento no es valida";
            if (age < 18)
            {
                return false;
            }
            return true;
        }

        public static bool ValidateIngresos(decimal ingresos, ref string message)
        {
            message = "El nivel de ingresos no es valido.";
            if (ingresos < 0 )
            {
                return false;
            }
            return true;
        }
        public static bool ValiadtePhone(int phone, ref string message)
        {
            message = "El celular no es valido";
            if (phone < 11111111 || phone > 999999999)
            {
                return false;
            }
            return true;
        }
        public static bool ValidateEmail(string email, ref string message)
        {
            string regex =  (@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,})+)$");

            message = "El correo no es valido";
            return Regex.IsMatch(email, regex, RegexOptions.IgnoreCase);
        }

        public static bool ValidateDate(DateTime  date, ref string message)
        {
            message = "La fecha no puede ser mayor a la fecha actual";
            if (date >= DateTime.Now.AddDays(1))
            {
                return false;
            }
            return true;
        }

    }
}
